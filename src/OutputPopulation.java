import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class OutputPopulation extends JFrame implements CustomIcon{
    private static OutputPopulation instance = null;

    private int FRAME_WIDTH = 300;
    private int FRAME_HEIGHT = 500;

    private JPanel contentPane;
    private JTable table;
    private JButton btnClose;

    public OutputPopulation(JFrame golFrame, ArrayList<Integer> populationOverTime) {
        setIconImage(icon.getImage());
        //setResizable(false);
        setTitle("Population");
        setContentPane(contentPane);
        getRootPane().setDefaultButton(btnClose);

        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        });
        // call onClose() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        // call onClose() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        updateData(populationOverTime);

        startGUI(golFrame);
    }

    public void updateData(ArrayList<Integer> populationOverTime)
    {
        //make numbers from 1 to size of array
        Integer[][] data = new Integer[populationOverTime.size()][2];
        for(int i = 0; i < populationOverTime.size(); i++)
        {
            data[i][0] = i+1;
            data[i][1] = populationOverTime.get(i);
        }
        String[] columnNames = {"Generation", "Population"};
        TableModel model = new DefaultTableModel(data, columnNames);

        table.setModel(model);
    }

    private void startGUI(JFrame golFrame)
    {
        //pack();
        contentPane.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        contentPane.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));

        ArrayList<String> helpMessage = new ArrayList<>();

        pack();
        setLocationRelativeTo(golFrame);
        setVisible(true);
    }

    public void onClose() {
        // add your code here
        instance = null;
        dispose();
    }

    public static OutputPopulation getInstance(JFrame parentFrame, ArrayList<Integer> populationOverTime)
    {
        if (instance == null)
            instance = new OutputPopulation(parentFrame, populationOverTime);

        return instance;
    }
}
