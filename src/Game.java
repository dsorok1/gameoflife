import java.awt.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class Game{
    private boolean gameDebug = false;
    private boolean resizeDebug = false;
    private boolean fillDebug = false;

    public static enum Ruleset{ABSORBING, ASTEROIDS, INFINITE, INVALID}

    private Ruleset rules = Ruleset.INVALID;

    private int dimension;
    private int width, height, area;
    private BigInteger areaFactorial;

    private int[] birthNeighbors;
    private int[] surviveNeighbors;

    private ArrayList<int[]> alivePoints = new ArrayList<>();
    private ArrayList<int[]> previewPoints = new ArrayList<>();
    //private GameObjectAnalyzer goa;

    public Game(int w, int h, Ruleset r)
    {
        setWidthHeight(w, h);
        //set birth and survive to be classic GOL
        setBirthNeighbors(new int[]{3});
        setSurviveNeighbors(new int[]{2, 3});
        setRules(r);
        //goa = new GameObjectAnalyzer(alivePoints);
    }

    public Game(int w, int h, int[] bN, int[] sN, Ruleset r)
    {
        setWidthHeight(w, h);
        setBirthNeighbors(bN);
        setSurviveNeighbors(sN);
        setRules(r);
        //goa = new GameObjectAnalyzer(alivePoints);
    }

    private void setWidthHeight(int w, int h)
    {
        width = w;
        height = h;
        area = w*h;
        areaFactorial = GameOfLife.factorial(area);
    }

    public void setRules(Ruleset r){rules = r;}

    public Ruleset getRules(){return rules;}

    public void setBirthNeighbors(int[] bN) {
        birthNeighbors = bN;
    }

    public void setSurviveNeighbors(int[] sN)
    {
        surviveNeighbors = sN;
    }

    public int getWidth() {return width;}

    public int getHeight() {return height;}

    public int getDimension() {return dimension;}

    public int getSmallestSide()
    {
        if(width > height)
            return height;
        return width;
    }

    public int getLargestSide()
    {
        if(width < height)
            return height;
        return width;
    }

    public int getArea() {return area;}

    public BigInteger getAreaFactorial() {return areaFactorial;}

    public void clearGame()
    {
        alivePoints.clear();
    }

    public void flipValue(int[] point)
    {
        if(isValidCoordinate(point))
        {//if coordinate is valid
            if(notAlreadyInPointArrayList(point, alivePoints))
            {//if is NOT in alive list yet
                alivePoints.add(point);
            }
            else
            {
                removeFromAliveIfExists(point);
            }
        }
    }

    //returns index of point in ArrayList
    private int arrayListOfIntArrContainsIndex(ArrayList<int[]> arrList, int[] point)
    {
        int dim = point.length;

        for(int entry = 0; entry < arrList.size(); entry++)
        {
            for(int d = 0; d < dim; d++)
            {
                if((arrList.get(entry))[d] == point[d])
                {
                    if(d == dim-1)
                    {
                        return entry;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        return -1;
    }

    private boolean arrayListOfIntArrContains(ArrayList<int[]> arrList, int[] point)
    {
        return arrayListOfIntArrContainsIndex(arrList, point) != -1;
    }

    private void removeFromAliveIfExists(int[] point)
    {
        int indexOfPoint = arrayListOfIntArrContainsIndex(alivePoints, point);
        if(indexOfPoint > -1)
            alivePoints.remove(indexOfPoint);
    }
    private void addToAliveIfNotAlreadyIn(int[] point)
    {
        if(!arrayListOfIntArrContains(alivePoints, point))
            alivePoints.add(point);
    }

    public void setDimension(int d)
    {
        if(d >= 2)
            dimension = d;
    }

    public void setTrue(int[] point)
    {
        if(isValidCoordinate(point))
            addToAliveIfNotAlreadyIn(point);
    }

    public void setFalse(int[] point)
    {
        if(isValidCoordinate(point))
            removeFromAliveIfExists(point);
    }

    public ArrayList<int[]> forwardStep()
    {
        //check each in alivePoints if they will survive

        ArrayList<int[]> nonAlivePointsToCheck = new ArrayList<>();
        //have to populate nAPTC with coordinates surrounding the alivePoints
        for(int iP = 0; iP < alivePoints.size(); iP++)
        {
            int x = alivePoints.get(iP)[0];
            int y = alivePoints.get(iP)[1];

            int xPrime = x;
            int yPrime = y;

            for(int direction = 0; direction < 8; direction ++) {
                switch(direction)
                {
                    case 0:
                        xPrime = x;
                        yPrime = y+1;
                        break;
                    case 1:
                        xPrime = x+1;
                        yPrime = y+1;
                        break;
                    case 2:
                        xPrime = x+1;
                        yPrime = y;
                        break;
                    case 3:
                        xPrime = x+1;
                        yPrime = y-1;
                        break;
                    case 4:
                        xPrime = x;
                        yPrime = y-1;
                        break;
                    case 5:
                        xPrime = x-1;
                        yPrime = y-1;
                        break;
                    case 6:
                        xPrime = x-1;
                        yPrime = y;
                        break;
                    case 7:
                        xPrime = x-1;
                        yPrime = y+1;
                        break;
                }

                int[] currentCoord = new int[]{xPrime, yPrime};

                if(rules == Ruleset.ASTEROIDS)
                    currentCoord = makeAsteroidCoord(currentCoord);

                if (isNotAliveOrRepeat(currentCoord, nonAlivePointsToCheck)) {
                    GameOfLife.printDebug(gameDebug, "Adding x = " + currentCoord[0] + " y = " + currentCoord[1] + " to nonAlivePointsToCheck");
                    nonAlivePointsToCheck.add(currentCoord);
                }
            }
        }
        //now nonAlivePointsToCheck is populated

        ArrayList<int[]> aliveNextStep = new ArrayList<>();

        for(int iAlive = 0; iAlive < alivePoints.size(); iAlive++)
        {//if it survives, add
            if(willSurvive(alivePoints.get(iAlive)))
            {
                GameOfLife.printDebug(gameDebug, alivePoints.get(iAlive)[0] + ", " + alivePoints.get(iAlive)[1] + " survives");
                aliveNextStep.add(alivePoints.get(iAlive));
            }
        }
        for(int iNAPTC = 0; iNAPTC < nonAlivePointsToCheck.size(); iNAPTC++)
        {
            if(willBeBorn(nonAlivePointsToCheck.get(iNAPTC)))
            {
                GameOfLife.printDebug(gameDebug, nonAlivePointsToCheck.get(iNAPTC)[0] + ", " + nonAlivePointsToCheck.get(iNAPTC)[1] + " is born");
                aliveNextStep.add(nonAlivePointsToCheck.get(iNAPTC));
            }
        }

        //now aliveNextStep holds future alive coordinates, set alivePoints to be it
        alivePoints = aliveNextStep;

        //now alivePoints is filled with all things that need to be true for next round

        return alivePoints;

    }

    public ArrayList<int[]> getCurrentAlivePoints()
    {
        return alivePoints;
    }

    private boolean isNotAliveOrRepeat(int[] point, ArrayList<int[]> points)
    {
        //      is valid coord               is not a repeat in the passed list                  is not in alive list
        return (isValidCoordinate(point) && notAlreadyInPointArrayList(point, points) && notAlreadyInPointArrayList(point, alivePoints));
    }

    //removed all instances of validation checking (except when drawing)
    private boolean isValidCoordinate(int[] point)
    {
        if(rules == Ruleset.INFINITE)
            return true;
        return (point[0] >= 0 && point[0] < width && point[1] >= 0 && point[1] < height);
    }

    private int[] makeAsteroidCoord(int[] point)
    {
        int x = point[0];
        int y = point[1];
        if(x < 0)
            x = width - 1;
        else if(x >= width)
            x = 0;

        if(y < 0)
            y = height - 1;
        else if(y >= height)
            y = 0;

        return new int[]{x, y};
    }

    private boolean notAlreadyInPointArrayList(int[] point, ArrayList<int[]> points)
    {

        return (!arrayListOfIntArrContains(points, point));
    }

    //returns the number of neighbors this coordinate has
    private int numNeighbors(int[] point)
    {
        int numNeighbors = 0;

        //TODO fix for n dimensions
        for(int addToDim1 = -1; addToDim1 <= 1; addToDim1++)
        {
            for(int addToDim2 = -1; addToDim2 <= 1; addToDim2++)
            {
                if(addToDim1 != 0 || addToDim2 != 0)
                {//don't count original point as neighbor
                    int[] currentPoint = new int[]{point[0] + addToDim1, point[1] + addToDim2};

                    GameOfLife.printDebug(gameDebug, "Checking neighbor " + currentPoint[0] + ", " + currentPoint[1] +
                            " for point " + point[0] + ", " + point[1]);

                    if(rules == Ruleset.ASTEROIDS)
                        currentPoint = makeAsteroidCoord(currentPoint);

                    numNeighbors += booleanToInt(
                            arrayListOfIntArrContains(
                                    alivePoints, currentPoint));
                }
            }
        }
        return numNeighbors;
    }
    private int booleanToInt(boolean tf)
    {
        return tf? 1: 0;
    }



    //function actually called
    //checks living blocks to see if they will continue to live
    private boolean willSurvive(int[] point)
    {
        int neighbors = numNeighbors(point);

        if(intArrContains(surviveNeighbors, neighbors))
        {//then, it has the right amount of neighbors to survive!
            GameOfLife.printDebug(gameDebug, point[0]+", "+point[1]+" survives, has " + neighbors + " neighbors");
            return true;
        }
        GameOfLife.printDebug(gameDebug, point[0]+", "+point[1]+" dies, has " + neighbors + " neighbors");
        return false;
    }

    //for (some) efficiency's sake, only check spots around populated blocks to see if they will be born
    private boolean willBeBorn(int[] point)
    {
        int neighbors = numNeighbors(point);

        if(intArrContains(birthNeighbors, neighbors))
        {//then, it has the right amount of neighbors to be born!
            GameOfLife.printDebug(gameDebug, point[0]+", "+point[1]+" is born, has " + neighbors + " neighbors");
            return true;
        }
        GameOfLife.printDebug(gameDebug, point[0]+", "+point[1]+" is not born, has " + neighbors + " neighbors");
        return false;
    }

    //helper for above functions
    private boolean intArrContains(int[] arr, int contains)
    {
        for(int i = 0; i < arr.length; i++)
        {
            if(arr[i] == contains)
                return true;
        }
        return false;
    }


    //when user updates screen size
    public void updateSettings(int w, int h)
    {
        setWidthHeight(w, h);

        //have to make sure all the current points adhere to this new width & height combo
        for(int iP = 0; iP < alivePoints.size(); iP++)
        {
            int[] currentPoint = alivePoints.get(iP);
            if(!(isValidCoordinate(currentPoint)))
            {//not valid coordinate anymore
                alivePoints.remove(iP);
                GameOfLife.printDebug(resizeDebug, "removed point");
                //this alters alivePoints' size, though
                iP--;
            }
        }
    }

    public void updateSettings(int w, int h, int[] bN, int[] sN)
    {
        setWidthHeight(w, h);
        setBirthNeighbors(bN);
        setSurviveNeighbors(sN);
    }

    private int posIntPower(int base, int exp)
    {
        int result = 1;
        for(int i = 0; i < exp; i++)
        {
            result *= base;
        }
        return result;
    }

    public void fillPercentage(int percent)
    {
        fillPercentage(percent, getSmallestSide());
    }

    public void fillPercentage(int percent, int sideLength)
    {
        GameOfLife.printDebug(fillDebug, "In fillPercentage");
        //have to make a percentage of game filled

        if(rules == Ruleset.ABSORBING || rules == Ruleset.ASTEROIDS)
        {//make sure sideLength is within the bounds
            sideLength = getSmallestSide();
        }

        int fillArea = posIntPower(sideLength, 2);
        GameOfLife.printDebug(fillDebug, "fillArea = " + fillArea);
        GameOfLife.printDebug(fillDebug, "percent = " + percent);
        int numberToFill = (int)(((double)percent / (double)100) * (double)fillArea);

        //will be filled with selected points and then returned
        ArrayList<int[]> pointsToSet = new ArrayList<>();

        //holds current unselected points
        ArrayList<int[]> selectablePoints = new ArrayList<>();

        int drawXOffset = 0;
        int drawYOffset = 0;
        if(sideLength < width)
        {//graph can fit within current graph bounds
            drawXOffset = (width - sideLength)/2;
        }
        if(sideLength < height)
        {
            drawYOffset = (height - sideLength)/2;
        }

        for(int w = drawXOffset; w < sideLength + drawXOffset; w++)
        {
            for(int h = drawYOffset; h < sideLength + drawYOffset; h++)
            {
                selectablePoints.add(new int[]{w, h});
            }
        }
        GameOfLife.printDebug(fillDebug, "selectablePoints starts out as " + selectablePoints);

        //rand num for loop
        Random rand = new Random();
        int randomNum;

        GameOfLife.printDebug(fillDebug, "numToFill = " + numberToFill);
        for(int i = 0; i < numberToFill; i++)
        {//select randomly and then remove from selectablePoints arrayList
            randomNum = rand.nextInt(selectablePoints.size());
            pointsToSet.add(selectablePoints.get(randomNum));
            selectablePoints.remove(randomNum);
        }

        alivePoints = pointsToSet;
        GameOfLife.printDebug(fillDebug, "leaving fillPercentage");
        GameOfLife.printDebug(fillDebug, "alivePoints = " + alivePoints);
    }

    public void setAlivePoints(ArrayList<int[]> ap)
    {
        alivePoints = ap;
    }
}
