import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;

public class ChangeSize extends JPanel implements CustomIcon{
    private boolean buttonDebug = false;
    private boolean changeSizeDebug = false;

    private JButton btnConfirm;
    private JFormattedTextField txtWidth;
    private JFormattedTextField txtHeight;
    private JFormattedTextField txtPixel;
    private JButton btnCancel;
    private JPanel mainPanel;
    private JFormattedTextField txtGraphWidth;
    private JFormattedTextField txtGraphHeight;
    private JLabel labelHeight;
    private JLabel labelPixel;
    private JPanel graphPanel;
    private JPanel gridPanel;

    private GameOfLife gol;
    private int gameWidth, gameHeight, multiplier, graphWidth, graphHeight;

    public static int MAX_SCREEN_WIDTH = 1500;
    public static int MAX_SCREEN_HEIGHT = 750;
    public static int MAX_PIXEL_SIZE = 50;

    public ChangeSize(GameOfLife g, int gw, int gh, int m, int grw, int grh)
    {
        gol = g;
        gameWidth = gw;
        gameHeight = gh;
        multiplier = m;

        graphWidth = grw;
        graphHeight = grh;


        //code allows enter to be pressed anywhere to activate btnConfirm
        txtWidth.addKeyListener(new KeyAdapter() {
            @Override public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnConfirm.doClick();
            }});
        txtHeight.addKeyListener(new KeyAdapter() {
            @Override public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnConfirm.doClick();
            }});
        txtPixel.addKeyListener(new KeyAdapter() {
            @Override public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnConfirm.doClick();
            }});
        txtGraphWidth.addKeyListener(new KeyAdapter() {
        @Override public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER)
                btnConfirm.doClick();
        }});
        txtGraphHeight.addKeyListener(new KeyAdapter() {
            @Override public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnConfirm.doClick();
            }});

        startGUI();
    }

    private void startGUI()
    {
        txtWidth.setText("" + gameWidth);
        txtHeight.setText("" + gameHeight);
        txtPixel.setText("" + multiplier);
        txtGraphWidth.setText("" + graphWidth);
        txtGraphHeight.setText("" + graphHeight);

        gridPanel.setBorder(new LineBorder(Color.GRAY));
        graphPanel.setBorder(new LineBorder(Color.GRAY));

        //JDialog frame = new JDialog(new JFrame("Dimensions"), Dialog.ModalityType.APPLICATION_MODAL);
        JDialog frame = new JDialog();
        frame.setTitle("Alter Dimensions");
        frame.setAlwaysOnTop(true);
        frame.setIconImage(icon.getImage());

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(gol.getFrame());
        frame.setResizable(false);
        frame.getRootPane().setDefaultButton(btnConfirm);

        //enable highlighting all on focus for the text fields
        txtWidth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e){
                txtWidth.setText(validateInputString(txtWidth.getText()));
            }
        });
        txtHeight.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e){
                txtHeight.setText(validateInputString(txtHeight.getText()));
            }
        });
        txtPixel.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e) {
                txtPixel.setText(validateInputString(txtPixel.getText()));
            }
        });
        txtGraphWidth.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e) {
                txtGraphWidth.setText(validateInputString(txtGraphWidth.getText()));
            }
        });
        txtGraphHeight.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e) {
                txtGraphHeight.setText(validateInputString(txtGraphHeight.getText()));
            }
        });

        //on update of txt fields, make sure it is a #, otherwise delete
        //NOT REALLY HOW I SHOULD DO THIS BUT FORMATTERS AREN'T PLAYING NICE!

        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                shutdown(frame);
            }
        });

        btnConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameOfLife.printDebug(buttonDebug, "btnConfirm");
                //first make sure that the sizes make sense
                updateSettings();
                shutdown(frame);
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameOfLife.printDebug(buttonDebug, "btnCancel");
                shutdown(frame);
            }
        });

        //frame
    }

    private String validateInputString(String input)
    {
        String outputString = "";
        //returns entire String of valid textBox input
        for(int i = 0; i < input.length(); i++)
        {
            if(isValidChar(input.charAt(i)))
                outputString += input.charAt(i);
        }
        return outputString;
    }
    private boolean isValidChar(char c)
    {
        return "1234567890".contains(""+c);
    }

    private void validateGameInput(String inputStrW, String inputStrH, String inputStrP)
    {
        int oldMult = multiplier;

        inputStrW = validateInputString(inputStrW);
        inputStrH = validateInputString(inputStrH);
        inputStrP = validateInputString(inputStrP);
        //now inputStr have either valid strings or nothing in them

        //directly change gameWidth and gameHeight

        //ABSOLUTE GAME WIDTH & HEIGHT refer to pixel size of screen
        //prioritize keeping pixel width FIRST, then change others
        if(!inputStrP.equals("")) {
            int inputP = Integer.parseInt(inputStrP);
            if (inputP <= 0)
                inputP = 1;
            else if (inputP > MAX_PIXEL_SIZE)
                inputP = MAX_PIXEL_SIZE;
            //inputP is a valid pixel size now
            multiplier = inputP;
        }

        if(!inputStrW.equals("")) {
            int inputW = Integer.parseInt(inputStrW);
            if (inputW <= 0)
                inputW = 1;
            else if (inputW * multiplier >= MAX_SCREEN_WIDTH)
                inputW = (MAX_SCREEN_WIDTH / multiplier);
            gameWidth = inputW;
        }
        else
        {//convert the old screen size to new units
            gameWidth = gameWidth * oldMult / multiplier;
        }

        if(!inputStrH.equals("")) {
            int inputH = Integer.parseInt(inputStrH);
            if (inputH <= 0)
                inputH = 1;
            else if (inputH * multiplier >= MAX_SCREEN_HEIGHT)
                inputH = (MAX_SCREEN_HEIGHT / multiplier);
            gameHeight = inputH;
        }
        else
        {
            gameHeight = gameHeight * oldMult / multiplier;
        }
    }

    private void validateGraphInput(String inputStrW, String inputStrH)
    {
        if(!inputStrW.equals("")) {
            int inputW = Integer.parseInt(inputStrW);
            if (inputW <= 0)
                inputW = 1;
            graphWidth = inputW;
        }//else, stays the same
        if(!inputStrH.equals("")) {
            int inputH = Integer.parseInt(inputStrH);
            if (inputH <= 0)
                inputH = 1;
            graphHeight = inputH;
        }//else, stays the same
    }

    //confirm button clicked
    private void updateSettings()
    {
        //have to change the settings to realistic ones, don't warn user, who cares

        validateGameInput(txtWidth.getText(),
                txtHeight.getText(),
                txtPixel.getText());
        validateGraphInput(txtGraphWidth.getText(), txtGraphHeight.getText());

        GameOfLife.printDebug(changeSizeDebug, "New width = " + gameWidth);
        GameOfLife.printDebug(changeSizeDebug, "New height = " + gameHeight);
        GameOfLife.printDebug(changeSizeDebug, "New size multiplier = " + multiplier);
        GameOfLife.printDebug(changeSizeDebug, "New graph width = " + graphWidth);
        GameOfLife.printDebug(changeSizeDebug, "New graph height = " + graphHeight);

        //validateGameInput changes the below variables directly
        gol.updateFromChangeSize(gameWidth, gameHeight, multiplier, graphWidth, graphHeight);
    }

    private void shutdown(JDialog frame)
    {
        gol.enableFrame();
        frame.dispose();
    }
}
