import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class GOLAutomater {

    private boolean automaterDebug = true;

    private int fillPercentage;
    private int maxGenerations;
    private int maxRuns;

    private Game asteroidsGame;
    private Game absorbingGame;
    private Game infiniteGame;

    private ArrayList<Integer> asteroidsGamePopulation;
    private ArrayList<Integer> absorbingGamePopulation;
    private ArrayList<Integer> infiniteGamePopulation;

    //below don't have run info
    private String asteroidsListNameBase;
    private String absorbingListNameBase;
    private String infiniteListNameBase;


    private String directory = "";

    //automates a number of rounds on a specific percent

    public GOLAutomater(int sideLength, int fillPercentage, int maxGenerations, int maxRuns, String directory)
    {
        asteroidsGamePopulation = new ArrayList<>();
        absorbingGamePopulation = new ArrayList<>();
        infiniteGamePopulation = new ArrayList<>();

        this.directory = directory;

        updateSettings(sideLength, fillPercentage);
        this.maxGenerations = maxGenerations;
        this.maxRuns = maxRuns;
    }

    public void updateSettings(int sideLength, int fillPercentage)
    {
        setGames(sideLength);
        this.fillPercentage = fillPercentage;

        setNameBase(sideLength, fillPercentage);
    }

    public void updateSettings(int fillPercentage)
    {
        int sideLength = asteroidsGame.getSmallestSide();
        setGames(sideLength);
        this.fillPercentage = fillPercentage;

        setNameBase(sideLength, fillPercentage);
    }

    private void setNameBase(int sideLength, int fillPercentage)
    {
        asteroidsListNameBase = "asteroids_side" + sideLength + "_fill" + fillPercentage + "_run";
        absorbingListNameBase = "absorbing_side" + sideLength + "_fill" + fillPercentage + "_run";
        infiniteListNameBase = "infinite_side" + sideLength + "_fill" + fillPercentage + "_run";
    }

    private void setGames(int sideLength)
    {
        asteroidsGame = null;
        absorbingGame = null;
        infiniteGame = null;

        asteroidsGame = new Game(sideLength, sideLength, Game.Ruleset.ASTEROIDS);
        absorbingGame = new Game(sideLength, sideLength, Game.Ruleset.ABSORBING);
        infiniteGame = new Game(sideLength, sideLength, Game.Ruleset.INFINITE);

        asteroidsGamePopulation.clear();
        absorbingGamePopulation.clear();
        infiniteGamePopulation.clear();
    }

    public void run()
    {
        String asteroidsListName = "";
        String absorbingListName = "";
        String infiniteListName = "";

        for(int run = 1; run <= maxRuns; run++)
        {
            asteroidsListName = makeFullListName(Game.Ruleset.ASTEROIDS, run);
            absorbingListName = makeFullListName(Game.Ruleset.ABSORBING, run);
            infiniteListName = makeFullListName(Game.Ruleset.INFINITE, run);

            asteroidsGame.clearGame();
            absorbingGame.clearGame();
            infiniteGame.clearGame();

            asteroidsGamePopulation.clear();
            absorbingGamePopulation.clear();
            infiniteGamePopulation.clear();

            asteroidsGame.fillPercentage(fillPercentage);
            absorbingGame.setAlivePoints(asteroidsGame.getCurrentAlivePoints());
            infiniteGame.setAlivePoints(asteroidsGame.getCurrentAlivePoints());

            //all have the same starting array

            capturePopulation();

            while(asteroidsGamePopulation.size() < maxGenerations)
            {//do step, then take population
                forwardStep();
                capturePopulation();
            }
            //reached maxGeneration, write population to file

            try
            {
                writeToFile(convertToPython(asteroidsGamePopulation, asteroidsListName), asteroidsListName);
                writeToFile(convertToPython(absorbingGamePopulation, absorbingListName), absorbingListName);
                writeToFile(convertToPython(infiniteGamePopulation, infiniteListName), infiniteListName);
            }catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(5);
            }

        }
    }

    private void capturePopulation()
    {
        asteroidsGamePopulation.add(asteroidsGame.getCurrentAlivePoints().size());
        absorbingGamePopulation.add(absorbingGame.getCurrentAlivePoints().size());
        infiniteGamePopulation.add(infiniteGame.getCurrentAlivePoints().size());
    }

    private void forwardStep()
    {
        asteroidsGame.forwardStep();
        absorbingGame.forwardStep();
        infiniteGame.forwardStep();
    }

    private void outputArrList(ArrayList arrList)
    {
        for(int i = 0; i < arrList.size(); i++)
        {
            System.out.println(arrList.get(i));
        }
    }

    private ArrayList<String> convertToPython(ArrayList arrList, String listName)
    {
        ArrayList<String> printableLines = new ArrayList<>();

        printableLines.add(listName + " = list()");
        for(int i = 0; i < arrList.size(); i++)
        {
            printableLines.add(listName + ".append(" + arrList.get(i) + ")");
        }

        return printableLines;
    }

    private String makeFullListName(Game.Ruleset rules, int run)
    {
        String listName = "";

        switch(rules)
        {
            case ASTEROIDS:
                listName = asteroidsListNameBase;
                break;
            case ABSORBING:
                listName = absorbingListNameBase;
                break;
            case INFINITE:
                listName = infiniteListNameBase;
                break;
        }

        return (listName + run);
    }

    private void writeToFile(ArrayList <String> printableLines, String fileName) throws IOException
    {
        BufferedWriter bw = new BufferedWriter(new FileWriter(directory + fileName + ".py"));
        for(int i = 0; i < printableLines.size(); i++)
        {
            bw.write(printableLines.get(i));
            bw.write("\n");
        }
        bw.close();
    }
}