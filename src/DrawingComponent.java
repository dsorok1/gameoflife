import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

//TODO: only update if it changed, none of this complete reload bullshit

public class DrawingComponent extends Canvas {

    private boolean drawDebug = false;
    private boolean hoverDebug = false;
    private boolean offsetDebug = false;
    private boolean scrollDebug = false;

    private Image dbImage;
    private int imageNumber = 1;

    private int gameWidth;
    private int gameHeight;
    private int multiplier;

    private int xOffset;
    private int yOffset;

    private int pixelX;
    private int pixelY;
    private boolean paintBlack;

    private int mouseX;
    private int mouseY;
    private int realX;
    private int realY;

    private Color gridColor = Color.lightGray;
    private boolean drawGrid = true;
    private boolean zoomCenter = false;

    private boolean load = true;//potential feature

    ArrayList<int[]> gameList = new ArrayList<>();


    public DrawingComponent(int gw, int gh, int m, int x, int y)
    {
        gameWidth = gw;
        gameHeight = gh;
        multiplier = m;

        //these 2 are used to move around graph's position
        xOffset = x;
        yOffset = y;
        clearMouseXY();
    }

    /**
        methods:
            update
            paint
        work together with
            paintComponent
        to create a buffered image to paint to screen
     **/

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        dbImage = createImage(getWidth(), getHeight());
        Graphics dbg = dbImage.getGraphics();
        paintComponent(dbg);
        g.drawImage(dbImage, 0, 0, this);

    }

    public void paintComponent(Graphics g)
    {
        GameOfLife.printDebug(drawDebug, "In paint method");
        //GameOfLife.printDebug(drawDebug, "load = " + load);

        //first clear background
        g.clearRect(0, 0, gameWidth * multiplier, gameHeight * multiplier);

        if(drawGrid) {
            g.setColor(gridColor);

            paintGrid(g);
        }

        g.setColor(Color.black);

        if(load)
        {//repaint entire picture

            for(int i = 0; i < gameList.size(); i++)
            {
                int[] currentPoint = gameList.get(i);
                paintPixelOffsetMultiplied(currentPoint[0], currentPoint[1], g);
            }

            GameOfLife.printDebug(hoverDebug, "mouseX mouseY = " + mouseX + ", " + mouseY);

            if(mouseX > -1 && mouseY > -1)
            {
                //draws preview of current pixel last
                g.setColor(Color.gray);
                paintPixelMultiplied(mouseX, mouseY, g);
            }
        }
        else
        {//just paint pixel that changed
            GameOfLife.printDebug(drawDebug, "In paint, load is disabled");
            if(paintBlack)
            {//paint current pixel black or gray
                paintPixelOffsetMultiplied(pixelX, pixelY, g);
            }
            else
            {//paint it white
                erasePixelOffsetMultiplied(pixelX, pixelY, g);
            }
        }
    }

    private void paintGrid(Graphics g)
    {
        //TODO: should really only calculate once then load every other time
        for(int x = 0; x < gameWidth; x++)
        {
            g.drawLine(multiplier * x, 0, multiplier * x, gameHeight * multiplier);
        }
        g.drawLine((multiplier * gameWidth) - 1, 0, (multiplier * gameWidth) - 1, gameHeight * multiplier);
        for(int y = 0; y < gameHeight; y++)
        {
            g.drawLine(0, multiplier * y, gameWidth * multiplier, multiplier * y);
        }
        g.drawLine(0, (multiplier * gameHeight) - 1, gameWidth * multiplier, (multiplier * gameHeight) - 1);
    }

    private void paintPixelMultiplied(int x, int y, Graphics g)
    {
        g.fillRect(multiplier * x, multiplier * y, multiplier, multiplier);
    }

    private void erasePixelMultiplied(int x, int y, Graphics g)
    {
        g.clearRect(multiplier * x, multiplier * y, multiplier, multiplier);
    }

    private void paintPixelOffsetMultiplied(int x, int y, Graphics g)
    {
        g.fillRect((x + (xOffset / multiplier)) * multiplier, (y + (yOffset / multiplier)) * multiplier, multiplier, multiplier);
    }

    private void erasePixelOffsetMultiplied(int x, int y, Graphics g)
    {
        g.clearRect((x + (xOffset / multiplier)) * multiplier, (y + (yOffset / multiplier)) * multiplier, multiplier, multiplier);
    }

    public void changeOffsetsBy(int x, int y)
    {
        xOffset += x;
        yOffset += y;
        GameOfLife.printDebug(offsetDebug, "xOffset = " + xOffset + ", yOffset = " + yOffset);
    }

    public void resetOffsets()
    {
        setOffsets(0, 0);
    }

    public void alignOffsets()
    {
        setOffsets((xOffset / multiplier) * multiplier, (yOffset / multiplier) * multiplier);
    }

    public void setOffsets(int x, int y)
    {
        xOffset = x;
        yOffset = y;
        GameOfLife.printDebug(offsetDebug, "xOffset = " + xOffset + ", yOffset = " + yOffset);
    }

    public int getXOffsetDivided()
    {
        return (xOffset / multiplier);
    }

    public int getYOffsetDivided()
    {
        return (yOffset / multiplier);
    }

    //gameList is a list of all colored in points
    public void setGame(ArrayList<int[]> inputGameList)
    {
        gameList = inputGameList;
    }

    public void setPixel(int inputX, int inputY, boolean blackOrWhite)
    {
        pixelX = inputX;
        pixelY = inputY;
        paintBlack = blackOrWhite;
    }

    public void setHover(int inputX, int inputY)
    {
        mouseX = inputX;
        mouseY = inputY;
    }

    public void setRealMousePosition(int rX, int rY)
    {
        realX = rX;
        realY = rY;
    }

    public void clearMouseXY()
    {
        mouseX = -1;
        mouseY = -1;
        realX = -1;
        realY = -1;
        repaint();
    }

    //called from other methods to change grid
    public void setDrawGrid(boolean tf)
    {
        drawGrid = tf;
    }

    public void setZoomCenter(boolean tf)
    {
        zoomCenter = tf;
    }

    public void updateSettings(int gw, int gh, int m)
    {
        gameWidth = gw;
        gameHeight = gh;
        multiplier = m;
    }

    public void scaleFromScroll(int gw, int gh, int m)
    {
        //depends on actual mouse position, offset, and scale
        //screen size stays the same

        //NEW PLAN! just zoom in/out on center

        int oldGameWidth = gameWidth;
        int oldGameHeight = gameHeight;
        int oldMultiplier = multiplier;

        //mouseX is also how many spaces are to the left
        //mouseY is how many spaces are above

        int middleXOnGrid = gameWidth / 2 - getXOffsetDivided();
        int middleYOnGrid = gameHeight / 2 - getYOffsetDivided();

        GameOfLife.printDebug(scrollDebug, "middleXOnGrid, middleYOnGrid before scrolling = (" + middleXOnGrid + ", " + middleYOnGrid + ")");

        int xOnGrid = mouseX - getXOffsetDivided();
        int yOnGrid = mouseY - getYOffsetDivided();


        //change offsets in a way such that the screen space does not move around mouse position

        GameOfLife.printDebug(scrollDebug, "mouseX, mouseY before scrolling = (" + mouseX + ", " + mouseY + ")");

        updateSettings(gw, gh, m);

        setHover(realX / multiplier, realY / multiplier);//update lil gray square

        if(!zoomCenter)
        {
            changeOffsetsBy((xOnGrid - mouseX + getXOffsetDivided()) * -multiplier, (yOnGrid - mouseY + getYOffsetDivided()) * -multiplier);
            GameOfLife.printDebug(offsetDebug, "offsets change by (" + xOffset + ", " + yOffset + ")");
        }
        else
        {
            setOffsets((gameWidth / 2 - middleXOnGrid) * multiplier, (gameHeight / 2 - middleYOnGrid) * multiplier);
        }

        //setHover(realX / multiplier, realY / multiplier);
    }

    public void enableLoad(){load = true;}

    public void disableLoad(){load = false;}

    public void saveImage(String directory)
    {
        try {
            BufferedImage saveImage = (BufferedImage) dbImage;
            File outputfile = new File(directory + "/" + "Board" + imageNumber + ".png");
            ImageIO.write(saveImage, "png", outputfile);
            imageNumber++;
        } catch (IOException e) {
        }
    }

}
