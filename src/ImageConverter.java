import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.util.ArrayList;

public class ImageConverter implements CustomIcon{

    private final int MAX_SIZE = 500;

    private boolean openImageDebug = false;
    private boolean convertImageDebug = false;

    private GameOfLife gol;
    private String filePath;
    private DrawingComponent canvas;
    private Game game;

    private JFrame frame;
    private ArrayList<int[]> aliveList;
    private int scaledW;
    private int scaledH;

    private BufferedImage image;

    private boolean invertColors = false;

    public ImageConverter(GameOfLife gol, String filePath, DrawingComponent canvas, Game game)
    {
        this.gol = gol;
        this.filePath = filePath;
        this.canvas = canvas;
        this.game = game;
        image = null;
        readImage();
    }

    public void readImage()
    {
        try
        {
            image = ImageIO.read(new File(filePath));
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        //TODO: clean up redundancies here, sloppy double conversion
        //TODO: crush images if they're too big
        //TODO: scale images if they're too small

        scaledW = image.getWidth();
        scaledH = image.getHeight();
        int largestDimension = scaledW;
        if(scaledH > scaledW)
            largestDimension = scaledH;

        float multiplyBy = 1;

        if(largestDimension > MAX_SIZE)
        {//rescale image to have smaller dimensions
            multiplyBy = ((float)MAX_SIZE / (float)largestDimension);

            scaledW *= multiplyBy;
            scaledH *= multiplyBy;
        }


        BufferedImage convertedImage = new BufferedImage(
                image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        GameOfLife.printDebug(openImageDebug, "Converted image: " + convertedImage.toString());

        Graphics2D g = convertedImage.createGraphics();
        g.setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR );
        g.drawImage(image, 0, 0, scaledW, scaledH, null);

        BufferedImage convertedImage2 = new BufferedImage(
                scaledW, scaledH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = convertedImage2.createGraphics();
        g2.drawImage(convertedImage, 0, 0, null);

        int width = image.getWidth();
        int height = image.getHeight();
        int area = width * height;

        boolean[] imageArr = new boolean[area];

        for(int w = 0 ; w < scaledW; w++ ) {
            for (int h = 0; h < scaledH; h++) {
                //GameOfLife.printDebug(openImageDebug, whiteOrBlack(convertedImage2.getRGB(w, h)) + "");
                imageArr[(width * h) + w] = whiteOrBlack(convertedImage2.getRGB(w, h));
            }
        }

        aliveList = convertBooleanArrayToAliveList(imageArr, width, height);

        //TODO: check size & stuff

        launchGUI(convertedImage2);

        if(convertImageDebug)
            testImage(convertedImage2);
    }

    private boolean whiteOrBlack(int num)
    {
        if(num < -1)
            return !invertColors;
        return invertColors;
    }

    private ArrayList<int[]> convertBooleanArrayToAliveList(boolean[] imageArr, int width, int height)
    {
        ArrayList<int[]> returnList = new ArrayList<>();

        for(int w = 0 ; w < width; w++ ) {
            for (int h = 0; h < height; h++) {

                if(imageArr[(width * h) + w])
                    returnList.add(new int[]{w, h});

            }
        }

        return returnList;
    }

    private void setGame(ArrayList<int[]> aliveList, int width, int height)
    {
        game.clearGame();
        game.updateSettings(width, height);
        game.setAlivePoints(aliveList);
    }

    private void testImage(BufferedImage bi)
    {
        JFrame f = new JFrame();
        JLabel l = new JLabel();
        l.setIcon(new ImageIcon(bi));
        f.getContentPane().add(l, BorderLayout.CENTER);
        f.pack();
        f.setVisible(true);
    }

    private void launchGUI(BufferedImage bi)
    {
        frame = new JFrame("Image Preview");
        JLabel picture = new JLabel();
        picture.setIcon(new ImageIcon(bi));
        JButton confirm = new JButton("Confirm");
        JButton cancel = new JButton("Cancel");
        JButton useAnother = new JButton("Use A Different Photo");

        confirm.addActionListener(e -> confirmSettings());
        cancel.addActionListener(e -> cancelSettings());
        useAnother.addActionListener(e -> openAnotherPhoto());

        JPanel panel = new JPanel(new GridBagLayout());
        JPanel imagePanel = new JPanel(new GridBagLayout());
        JPanel controlsPanel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx=0;
        gbc.gridy=0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=2;
        imagePanel.add(picture, gbc);

        gbc.gridx=0;
        gbc.gridy=0;
        gbc.gridwidth=1;
        controlsPanel.add(confirm, gbc);
        gbc.gridx=1;
        controlsPanel.add(cancel, gbc);
        gbc.gridx=2;
        controlsPanel.add(useAnother, gbc);

        gbc.gridx=0;
        gbc.gridy=0;
        panel.add(imagePanel, gbc);
        gbc.gridy=1;
        panel.add(controlsPanel, gbc);

        frame.getContentPane().add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setIconImage(icon.getImage());
        frame.setLocationRelativeTo(gol.getFrame());
        frame.setVisible(true);
    }

    private void confirmSettings()
    {
        frame.dispose();
        setGame(aliveList, scaledW, scaledH);
        gol.updateFromImageConverter(2);
    }

    private void cancelSettings()
    {
        frame.dispose();
    }

    private void openAnotherPhoto()
    {
        frame.dispose();
        gol.launchOpenImage();
    }
}
