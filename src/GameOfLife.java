import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/*
    TODO: add fractal dimension

    TODO: implement multithreading during calculation (alive/dead maybe...)
    TODO: add detection of still life, oscillators, and gliders & autoStop feature
        group by adjacent blocks & search

    TODO: possibly add saving
 */

public class GameOfLife implements CustomIcon{
    
    private boolean golDebug = false;
    private boolean tweensDebug = false;
    private boolean guiDebug = false;
    private boolean sliderDebug = false;
    private boolean settingsDebug = false;
    private boolean populationDebug = false;
    private boolean fillDebug = false;
    private boolean multiplicityDebug = false;

    private String startTime = "" + System.nanoTime();
    private boolean saveOutput = false;
    private String outputDirectory = System.getProperty("user.home") +
            System.getProperty("file.separator") +
            "Documents" + System.getProperty("file.separator") +
            "_GOL_STUFF" + System.getProperty("file.separator") +
            "Images" + System.getProperty("file.separator") + startTime;

    private boolean allowZoom = false;
    private boolean allowMove = false;

    private boolean drawingMode = true;

    private int FRAME_WIDTH = 500;
    private int FRAME_HEIGHT = 500;
    private final int BUTTON_HEIGHT = 100;
    private int MIN_TIME_RATE = 0;//in Hz
    private int MAX_TIME_RATE = 50;//in Hz
    private int TIME_STEP = 40;//in ms
    private int SIZE_MULTIPLIER = 5;
    private int SCROLL_MULTIPLIER = 1;
    private int MIN_GAME_WIDTH = 5;


    private int GRAPH_WIDTH = 350;
    private int GRAPH_HEIGHT = 350;
    private int GRAPH_MARGIN = 50;
    private int POPULATION_MAXIMUM = 2000;
    private String TXT_DISP_MARGIN = "";
    private int DEFAULT_FILL_SIZE = 100;
    private int DEFAULT_FILL_PERCENTAGE = 20;

    private boolean running = false;
    private ArrayList<Integer> populationOverTime = new ArrayList<>();
    private int maxPop, minPop, currentPop;
    private int populationHasBeenSameFor;
    private int POPULATION_REEAT_CUTOFF;
    private boolean drawGraph;
    private boolean showCoords;
    private boolean zoomCenter;

    private OutputPopulation outputPop = null;

    private Game game;
    private Timer timer;

    //static Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource());
    //static ImageIcon icon = new ImageIcon("resources/small_arbuckle.png");

    private JFrame frame;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu settingsMenu;
    private JCheckBoxMenuItem absItem;
    private JCheckBoxMenuItem astItem;
    private JCheckBoxMenuItem infItem;
    private JButton btnHelp;
    private JCheckBoxMenuItem gridItem;
    private JCheckBoxMenuItem graphItem;
    private JCheckBoxMenuItem coordsItem;
    private JCheckBoxMenuItem zoomItem;
    private JPanel panel;
    private JButton btnClear;
    private JButton btnStep;
    private JButton btnPlay;
    private JButton btnOutputPopulation;
    private JButton btnClearGraph;
    private JTextField txtFillSize;
    private JTextField txtFillPercentage;
    private JButton btnFillPercentage;
    private JLabel rateLabel;
    private JSlider rate;
    private Canvas canvas;
    private Canvas graphCanvas;

    private JLabel txtMaxPop;
    private JLabel txtMaxGen;
    private JLabel txtMinPop;
    private JLabel txtMinGen;
    private JLabel txtInitialPop;
    private JLabel txtCurrGen;
    private JLabel txtCurrPop;

    public static void main(String [] args)
    {
        //TODO: implement with command line arguments

        GameOfLife gol = new GameOfLife();
        gol.startGUI(null);

        if(gol.saveOutput)
        {
            new File(gol.outputDirectory).mkdir();
        }
/*
        long startTime = System.nanoTime();
        long mostRecentTime = startTime;

        String directory = "/home/desmond/Documents/GOL_data/7-5-19/data/";

        int percentStep = 10;
        int sideStep = 10;
        int maxSide = 100;

        GOLAutomater robot = new GOLAutomater(sideStep, 0, 500, 30, directory);

        //still not using parallelism :/

        for(int sideLength = sideStep; sideLength <= maxSide; sideLength += sideStep)
        {
            for (int percent = percentStep; percent < 100; percent += percentStep)
            {
                robot.updateSettings(sideLength, percent);
                robot.run();

                long currentTime = System.nanoTime();

                System.out.println("Done " + sideLength + " x " + sideLength + " " +
                        + percent + "%. Took " + ((double) (currentTime - mostRecentTime) / 1_000_000_000) + " seconds.");

                mostRecentTime = currentTime;
            }
        }

        long endTime = System.nanoTime();
        long duration = (endTime - startTime);

        double durationSeconds = (double) duration / 1_000_000_000;

        System.out.println(durationSeconds + " seconds");
*/
    }

    private GameOfLife()
    {
        timer = new Timer(TIME_STEP, null);
        timer.addActionListener(e -> timerActionPerformed());
        timer.setRepeats(true);

        showCoords = false;
        drawGraph = true;
        zoomCenter = false;


        game = new Game(FRAME_WIDTH / SIZE_MULTIPLIER, FRAME_HEIGHT / SIZE_MULTIPLIER, Game.Ruleset.ABSORBING);

        canvas = new DrawingComponent(game.getWidth(), game.getHeight(), SIZE_MULTIPLIER, 0, 0);
        canvas.setSize(FRAME_WIDTH, FRAME_HEIGHT);

        gridItem = new JCheckBoxMenuItem("Grid");
        gridItem.setState(true);

        graphItem = new JCheckBoxMenuItem("Graph Population");
        graphItem.setState(drawGraph);

        coordsItem = new JCheckBoxMenuItem("Show Coords");
        coordsItem.setState(false);

        zoomItem = new JCheckBoxMenuItem("Zoom About Cursor");
        zoomItem.setState(!zoomCenter);

        txtFillSize = new JTextField(""+DEFAULT_FILL_SIZE, 2);
        txtFillPercentage = new JTextField(""+DEFAULT_FILL_PERCENTAGE, 2);

        absItem = new JCheckBoxMenuItem("Absorbing Boundaries");
        absItem.setState(true);

        astItem = new JCheckBoxMenuItem("Asteroids Boundaries");
        astItem.setState(false);

        infItem = new JCheckBoxMenuItem("Infinite Boundaries");
        infItem.setState(false);



        //below button definitions have to be here to stop bugs from spawning many GUIs

        //settings checkbox that draws/undraws gray grid on click
        gridItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                printDebug(settingsDebug, "Grid Clicked");
                ((DrawingComponent) canvas).setDrawGrid(gridItem.isSelected());
                //then repaint current screen
                canvas.repaint();
            }
        });
        //settings checkbox that draws/undraws graph on click
        graphItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                printDebug(settingsDebug, "Graph Clicked");
                stopLoop();
                drawGraph = graphItem.isSelected();
                populationOverTime.clear();

                Point loc = frame.getLocation();
                frame.dispose();
                startGUI(loc);
            }
        });
        coordsItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                showCoords = coordsItem.isSelected();
                if(showCoords)
                    frame.setTitle("Game of Life | ");
                else
                    frame.setTitle("Game of Life");
            }
        });
        zoomItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                zoomCenter = !zoomItem.isSelected();
                ((DrawingComponent) canvas).setZoomCenter(zoomCenter);
            }
        });

        txtFillSize.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e) {
                txtFillSize.setText(validateFillSizeInputString(txtFillSize.getText()));
            }
        });

        txtFillPercentage.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ((JTextField) e.getSource()).selectAll();
            }
            @Override
            public void focusLost(FocusEvent e) {
                txtFillPercentage.setText(validateInputString(txtFillPercentage.getText()));
            }
        });

        gridItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                printDebug(settingsDebug, "Grid Clicked");
                ((DrawingComponent) canvas).setDrawGrid(gridItem.isSelected());
                //then repaint current screen
                canvas.repaint();
            }
        });

        absItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Abs Clicked");
                game.setRules(Game.Ruleset.ABSORBING);
                absItem.setState(true);
                astItem.setState(false);
                infItem.setState(false);
                allowMove = false;
                allowZoom = false;
            }
        });

        astItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Ast Clicked");
                game.setRules(Game.Ruleset.ASTEROIDS);
                absItem.setState(false);
                astItem.setState(true);
                infItem.setState(false);
                allowMove = false;
                allowZoom = false;
            }
        });

        infItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Inf Clicked");
                game.setRules(Game.Ruleset.INFINITE);
                absItem.setState(false);
                astItem.setState(false);
                infItem.setState(true);
                allowMove = true;
                allowZoom = true;
            }
        });

    }

    private void startGUI(Point loc)
    {

        //drawGraph
        if(drawGraph) {
            if(graphCanvas == null) {
                graphCanvas = new DrawingGraph(GRAPH_WIDTH, GRAPH_HEIGHT, GRAPH_MARGIN);
                graphCanvas.setSize(GRAPH_WIDTH, GRAPH_HEIGHT);
            }
            else {
                graphCanvas.setSize(GRAPH_WIDTH, GRAPH_HEIGHT);
                graphCanvas.repaint();
            }
        }



        menuBar = new JMenuBar();

        fileMenu = new JMenu("File");

        JMenuItem saveItem = new JMenuItem(new AbstractAction("Save") {
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Save Clicked");
                launchSaveBoard();
            }
        });

        JMenuItem loadItem = new JMenuItem(new AbstractAction("Load") {
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Load Clicked");
                launchLoadBoard();
            }
        });

        fileMenu.add(saveItem);
        fileMenu.add(loadItem);

        settingsMenu = new JMenu("Settings");

        JMenuItem changeSizeItem = new JMenuItem(new AbstractAction("Alter Dimensions") {
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Change Size Clicked");
                launchChangeSize();
            }
        });

        settingsMenu.add(changeSizeItem);

        settingsMenu.addSeparator();

        settingsMenu.add(absItem);
        settingsMenu.add(astItem);
        settingsMenu.add(infItem);

        settingsMenu.addSeparator();


        settingsMenu.add(gridItem);

        settingsMenu.add(coordsItem);

        if(allowZoom)
            settingsMenu.add(zoomItem);

        settingsMenu.addSeparator();

        settingsMenu.add(graphItem);

        settingsMenu.addSeparator();

        JMenuItem openImageItem = new JMenuItem(new AbstractAction("Open Image") {
            public void actionPerformed(ActionEvent e) {
                printDebug(settingsDebug, "Open Image Clicked");
                launchOpenImage();
            }
        });

        settingsMenu.add(openImageItem);



        //menuBar.add(new JSeparator());
        menuBar.add(fileMenu);
        menuBar.add(settingsMenu);

        btnHelp = new JButton("Help");
        btnHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                launchHelp();
            }
        });

        menuBar.add(btnHelp);




        frame = new JFrame("Game of Life");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT + BUTTON_HEIGHT);
        frame.setLocationRelativeTo(null);
        if(loc != null)
            frame.setLocation(loc);
        frame.setVisible(true);
        //frame.setResizable(false);
        frame.setIconImage(icon.getImage());

        frame.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent componentEvent) {
                if(graphCanvas != null)
                {
                    graphCanvas.repaint();
                }
                canvas.repaint();
            }
        });

        panel = new JPanel();

        btnStep = new JButton("Step");
        btnStep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateGraphicsStep();
            }
        });

        btnPlay = new JButton("Start");
        btnPlay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDebug(guiDebug, "Button clicked");
                if(!running)
                {
                    startLoop();
                }
                else
                {
                    stopLoop();
                    //changeRunning();
                }
            }
        });

        //TODO: add label above rate using vertical panel
        rateLabel = new JLabel();
        updateRateLabel(1000/TIME_STEP);
        rate = new JSlider(JSlider.HORIZONTAL, MIN_TIME_RATE, MAX_TIME_RATE, 1000/TIME_STEP);
        rate.setMajorTickSpacing((MAX_TIME_RATE - MIN_TIME_RATE)/5);
        rate.setMinorTickSpacing((MAX_TIME_RATE - MIN_TIME_RATE)/10);
        rate.setPaintTicks(true);
        rate.setPaintLabels(true);
        rate.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                printDebug(sliderDebug, "Current slider value = " + rate.getValue());
                setTimeStep(rate.getValue());
                updateRateLabel(rate.getValue());
            }
        });

        btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearGraphics();
                ((DrawingComponent) canvas).resetOffsets();
                //also stop if running
                stopLoop();
            }
        });

        btnOutputPopulation = new JButton("Output Population");
        btnOutputPopulation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                launchOutputPopulation();
            }
        });

        btnClearGraph = new JButton("Clear Graph");
        btnClearGraph.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearGraphGraphics();
            }
        });

        btnFillPercentage = new JButton("Fill Percentage");
        btnFillPercentage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((DrawingComponent) canvas).resetOffsets();
                fillPercentage();
            }
        });


        MouseAdapter ma = new MouseAdapter()
        {

            private Point lastPoint = new Point();

            private boolean paintCurrentPathBlack = true;//used to see whether you're painting or erasing

            @Override
            public void mouseMoved(MouseEvent e)
            {
                if(showCoords)
                    handleMouseMoving(e.getX()/SIZE_MULTIPLIER, e.getY()/SIZE_MULTIPLIER);
                //draw preview of pixel
                if(!(SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e)) ) {//&& !running
                    hoverCoord(e.getX(), e.getY());
                }
            }

            //gets the position of click
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                printDebug(guiDebug, x + ", " + y);

                ((DrawingComponent) canvas).clearMouseXY();

                if(SwingUtilities.isLeftMouseButton(e))
                {
                    clickedCoord(x, y, true);
                }
                else
                {//if not left click, just erase
                    clickedCoord(x, y, false);
                }

                if(!(SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e)) ) {//&& !running
                    hoverCoord(e.getX(), e.getY());
                }
            }

            @Override
            public void mousePressed(MouseEvent e)
            {
                paintCurrentPathBlack = SwingUtilities.isLeftMouseButton(e);
                //above will be true if left click, false if else

                lastPoint = e.getPoint();
            }
            @Override
            public void mouseDragged(MouseEvent e)
            {
                super.mouseDragged(e);

                //remove gray pixel
                ((DrawingComponent) canvas).clearMouseXY();

                Point pointDragged = new Point(e.getX(), e.getY());

                if(drawingMode)
                {
                    if (showCoords)
                        handleMouseMoving(pointDragged.x / SIZE_MULTIPLIER, pointDragged.y / SIZE_MULTIPLIER);

                    //check to see if it skipped pixels and interpolate (if showing on screen currently)

                    /*if(pointDragged.x / SIZE_MULTIPLIER <= game.getWidth()
                            && pointDragged.x >= 0
                            && pointDragged.y / SIZE_MULTIPLIER <= game.getHeight()
                            && pointDragged.y >= 0)*/
                        drawPixels(interpolatePoints(lastPoint, pointDragged, true), paintCurrentPathBlack);
                }
                else
                {//NOT drawing mode, so move background
                    ((DrawingComponent) canvas).changeOffsetsBy(pointDragged.x - lastPoint.x, pointDragged.y - lastPoint.y);
                    canvas.repaint();
                }
                printDebug(guiDebug, "Mouse dragged to x = " + e.getX() + ", y = " + e.getY());
                lastPoint = pointDragged;
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                lastPoint = null;
                if(!running)
                    hoverCoord(e.getX(), e.getY());
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                ((DrawingComponent) canvas).clearMouseXY();

            }
        };
        MouseWheelListener ml = new MouseWheelListener()
        {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if(allowZoom)
                    updateFromScroll(e.getWheelRotation() > 0);
            }
        };

        KeyListener kl = new KeyListener() {
            @Override public void keyTyped(KeyEvent e){}

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == 32)
                {//space bar
                    if(allowMove)
                        drawingMode = false;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == 32)
                {//space bar
                    if(allowMove)
                        drawingMode = true;
                }
            }
        };

        if(canvas.getMouseListeners().length == 0)
            canvas.addMouseListener(ma);
        if(canvas.getMouseMotionListeners().length == 0)
            canvas.addMouseMotionListener(ma);
        if(canvas.getMouseWheelListeners().length == 0)
            canvas.addMouseWheelListener(ml);
        if(canvas.getKeyListeners().length == 0)
            canvas.addKeyListener(kl);

        JPanel picturePanel = new JPanel();
        picturePanel.setBorder(new LineBorder(Color.BLACK));

        //allows for border around drawing area
        picturePanel.add(canvas, BorderLayout.CENTER);

        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel sliderPanel = new JPanel();
        sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.Y_AXIS));
        sliderPanel.add(rateLabel);
        sliderPanel.add(rate);


        frame.setJMenuBar(menuBar);


        JPanel startPanel = new JPanel();
        startPanel.add(btnPlay);
        startPanel.add(sliderPanel);

        gbc.gridx=0;
        gbc.gridy=0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth=3;
        //gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(picturePanel, gbc);

        if(drawGraph)
        {
            JPanel graphPanel = new JPanel();
            graphPanel.setLayout(new GridBagLayout());

            GridBagConstraints gbcGraph = new GridBagConstraints();
            gbcGraph.weightx = 1;
            gbcGraph.weighty = 1;
            gbcGraph.gridx=0;
            gbcGraph.gridy=0;
            gbcGraph.gridwidth=2;
            gbcGraph.gridheight=1;
            gbcGraph.insets = new Insets(0,0,0,0);
            graphPanel.add(new JLabel("Population vs Generation"), gbcGraph);

            gbcGraph.gridx=0;
            gbcGraph.gridy=1;
            graphPanel.add(graphCanvas, gbcGraph);

            gbcGraph.gridwidth=2;
            gbcGraph.gridx=0;
            gbcGraph.gridy=4;
            gbcGraph.insets = new Insets(10,0,0,0);
            graphPanel.add(btnOutputPopulation, gbcGraph);

            gbcGraph.gridy=5;
            graphPanel.add(btnClearGraph, gbcGraph);

            //initial stuff
            JPanel initialPanel = new JPanel();
            initialPanel.setLayout(new GridBagLayout());

            gbcGraph.gridwidth=1;
            gbcGraph.gridheight=1;
            gbcGraph.gridx=0;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5,5,5,0);
            initialPanel.add(new JLabel("Initial pop: "), gbcGraph);

            txtInitialPop = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5,0,5,5);
            initialPanel.add(txtInitialPop, gbcGraph);

            initialPanel.setBorder(new LineBorder(Color.lightGray));

            gbcGraph.gridx=0;
            gbcGraph.gridy=2;
            gbcGraph.insets = new Insets(0,0,10,0);
            graphPanel.add(initialPanel, gbcGraph);


            //current stuff

            JPanel currentPanel = new JPanel();
            currentPanel.setLayout(new GridBagLayout());

            gbcGraph.gridwidth=1;
            gbcGraph.gridheight=1;
            gbcGraph.gridx=0;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5,5,5,0);
            currentPanel.add(new JLabel("Current pop: "), gbcGraph);

            txtCurrPop = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5,0,5,5);
            currentPanel.add(txtCurrPop, gbcGraph);

            gbcGraph.gridwidth=1;
            gbcGraph.gridheight=1;
            gbcGraph.gridx=0;
            gbcGraph.gridy=1;
            gbcGraph.insets = new Insets(0,5,5,0);
            currentPanel.add(new JLabel("Current gen: "), gbcGraph);

            txtCurrGen = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=1;
            gbcGraph.insets = new Insets(0,0,5,5);
            currentPanel.add(txtCurrGen, gbcGraph);

            currentPanel.setBorder(new LineBorder(Color.lightGray));

            gbcGraph.gridx=1;
            gbcGraph.gridy=2;
            gbcGraph.insets = new Insets(0,0,10,0);
            graphPanel.add(currentPanel, gbcGraph);



            gbcGraph.gridwidth=1;
            gbcGraph.gridheight=1;

            JPanel popStatsPanel = new JPanel();
            popStatsPanel.setLayout(new GridBagLayout());
            gbcGraph.gridx=0;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5,5,5,0);
            popStatsPanel.add(new JLabel("Max pop: "), gbcGraph);

            txtMaxPop = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=0;
            gbcGraph.insets = new Insets(5, 0,5, 5);
            popStatsPanel.add(txtMaxPop, gbcGraph);

            gbcGraph.gridx=0;
            gbcGraph.gridy=1;
            gbcGraph.insets = new Insets(0,5,5,0);
            popStatsPanel.add(new JLabel("      At gen: "), gbcGraph);

            txtMaxGen = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=1;
            gbcGraph.insets = new Insets(0,0,5,5);
            popStatsPanel.add(txtMaxGen, gbcGraph);

            gbcGraph.gridx=0;
            gbcGraph.gridy=2;
            gbcGraph.insets = new Insets(10,5,5,0);
            popStatsPanel.add(new JLabel("Min pop: "), gbcGraph);

            txtMinPop = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=2;
            gbcGraph.insets = new Insets(10,0,5,5);
            popStatsPanel.add(txtMinPop, gbcGraph);

            gbcGraph.gridx=0;
            gbcGraph.gridy=3;
            gbcGraph.insets = new Insets(0,5,5,0);
            popStatsPanel.add(new JLabel("      At gen: "), gbcGraph);

            txtMinGen = new JLabel(TXT_DISP_MARGIN);
            gbcGraph.gridx=1;
            gbcGraph.gridy=3;
            gbcGraph.insets = new Insets(0,0,5,5);
            popStatsPanel.add(txtMinGen, gbcGraph);

            popStatsPanel.setBorder(new LineBorder(Color.DARK_GRAY));


            gbcGraph.gridwidth=2;
            gbcGraph.gridheight=1;
            gbcGraph.gridx=0;
            gbcGraph.gridy=3;
            gbcGraph.insets = new Insets(0,0,0,0);
            graphPanel.add(popStatsPanel, gbcGraph);



            gbc.gridx=4;
            gbc.gridy=0;
            gbc.gridwidth=1;
            gbc.gridheight=2;
            panel.add(graphPanel, gbc);
        }

        gbc.gridx=0;
        gbc.gridy=1;
        gbc.gridwidth=1;
        gbc.gridheight=1;
        panel.add(btnStep, gbc);
        gbc.gridx=1;
        gbc.gridy=1;
        panel.add(startPanel, gbc);
        gbc.gridx=2;
        gbc.gridy=1;
        panel.add(btnClear, gbc);

        JPanel fillPercentagePanelLeft = new JPanel();
        fillPercentagePanelLeft.setLayout(new GridBagLayout());
        gbc.gridx=0;
        gbc.gridy=0;
        fillPercentagePanelLeft.add(new JLabel("Fill side length:"), gbc);
        gbc.gridx=1;
        gbc.gridy=0;
        fillPercentagePanelLeft.add(txtFillSize, gbc);
        JPanel fillPercentagePanelRight = new JPanel();
        fillPercentagePanelRight.setLayout(new GridBagLayout());
        gbc.gridx=0;
        gbc.gridy=0;
        fillPercentagePanelRight.add(txtFillPercentage, gbc);
        gbc.gridx=1;
        gbc.gridy=0;
        fillPercentagePanelRight.add(btnFillPercentage, gbc);

        gbc.gridx=0;
        gbc.gridy=2;
        gbc.insets = new Insets(8,0,16,0);
        panel.add(fillPercentagePanelLeft, gbc);
        gbc.gridx=1;
        gbc.gridy=2;
        panel.add(fillPercentagePanelRight, gbc);

        frame.add(panel, BorderLayout.CENTER);


        frame.pack();
        frame.setMinimumSize(frame.getSize());

        ((DrawingComponent) canvas).setGame(game.getCurrentAlivePoints());
    }


    private void startLoop()
    {
        btnStep.setEnabled(false);
        running = true;
        btnPlay.setText("Stop");
        ((DrawingComponent) canvas).clearMouseXY();
        timer.start();
    }

    private void stopLoop()
    {
        btnStep.setEnabled(true);
        running = false;
        btnPlay.setText("Start");
    }

    //called when the mouse is moved
    private void handleMouseMoving(int x, int y)
    {
        if(x >= 0 && x < FRAME_WIDTH/SIZE_MULTIPLIER && y >= 0 && y < FRAME_HEIGHT/SIZE_MULTIPLIER) {
            x -= getXOffsetDivided();
            y -= getYOffsetDivided();
            frame.setTitle("Game of Life | " + x + ", " + y);
        }
    }

    private void setTimeStep(int jsValue)
    {
        if(jsValue <= 0)
        {
            //stopLoop();
            //btnPlay.setEnabled(false);
            //instead of stopping loop, just set to 1
            rate.setValue(1);
        }
        else
        {
            //btnPlay.setEnabled(true);
            TIME_STEP = (1000 / jsValue);
            printDebug(sliderDebug, "Current TIME_STEP value = " + TIME_STEP);
            timer.setDelay(TIME_STEP);
        }
    }

    //mouse can move too fast for screen to handle, smooths out mouse path
    private ArrayList<Point> interpolatePoints(Point pastP, Point currentP, boolean shouldIncludeEndpoints)
    {
        //returns arrayList of points in between other points
        ArrayList<Point> tweens = new ArrayList<>();

        //have to divide by multiplier to not count needless points
        int pastX = pastP.x/SIZE_MULTIPLIER;
        int pastY = pastP.y/SIZE_MULTIPLIER;
        int currentX = currentP.x/SIZE_MULTIPLIER;
        int currentY = currentP.y/SIZE_MULTIPLIER;

        if(!areAdjacent(pastX, pastY, currentX, currentY))
        {//not adjacent, fill points
            printDebug(tweensDebug, "Points are not adjacent");
            printDebug(tweensDebug, "x1, y1 = ("+ pastX +","+ pastY +") x2, y2 = ( "+ currentX +", "+ currentY +" )");

            float slope = ((float)(currentY-pastY))/((float)(currentX-pastX));
            float intercept;

            if(Math.abs(slope) <= 1.0f)
            {//if slope is more horizontal than vertical use this method
                //find equation for line

                int lowX = pastX;
                int highX = currentX;

                if(pastX > currentX) {
                    lowX = currentX;
                    highX = pastX;
                }

                intercept = (float)pastY - slope * ((float)pastX);

                //printDebug(tweensDebug, "lowX+1 = " + (lowX+1) + ", highX = " + highX);
                for(int x = (lowX+1); x < highX; x++)
                {
                    //slope intercept doesnt work for vertical lines. duh.
                    float y = slope * ((float)x) + intercept;
                    tweens.add(new Point(x * SIZE_MULTIPLIER, roundFloat(y) * SIZE_MULTIPLIER));
                }
            }
            else
            {

                int lowY = pastY;
                int highY = currentY;

                if(pastY > currentY) {
                    lowY = currentY;
                    highY = pastY;
                }

                slope = ((float)(currentX-pastX))/((float)(currentY-pastY));;
                intercept = (float)pastX - slope * ((float)pastY);

                printDebug(tweensDebug, "lowY+1 = " + (lowY+1) + ", highY = " + highY);
                for(int y = (lowY+1); y < highY; y++)
                {
                    float x = slope * ((float)y) + intercept;
                    tweens.add(new Point(roundFloat(x) * SIZE_MULTIPLIER, y * SIZE_MULTIPLIER));
                }
            }
        }
        //printDebug(tweensDebug, "Returning tweens = " + tweens);

        if(shouldIncludeEndpoints)
        {
            tweens.add(pastP);
            tweens.add(currentP);
        }

        return tweens;
    }

    //rounds a float up or down
    private int roundFloat(float input)
    {
        if(input - (float)((int)input) < 0.5f)
        {//round down
            return (int)input;
        }
        //if not, round up
        return (((int)input) + 1);

    }

    //returns true if 2 grid points are adjacent
    private boolean areAdjacent(int x1, int y1, int x2, int y2)
    {
        return (Math.abs(x1 - x2) <= 1) && (Math.abs(y1 - y2) <= 1);
    }

    private void clearGraphics()
    {
        //((DrawingComponent) canvas).enableLoad();
        game.clearGame();
        canvas.repaint();

        //have to clear graph
        if(drawGraph) {
            clearGraphGraphics();
        }

        ((DrawingComponent) canvas).clearMouseXY();
        //change later
        //frame.setTitle("Game of Life");
    }

    private void fillPercentage()
    {
        clearGraphics();
        stopLoop();
        game.fillPercentage(Integer.parseInt(txtFillPercentage.getText()), Integer.parseInt(txtFillSize.getText()));
        ((DrawingComponent) canvas).setGame(game.getCurrentAlivePoints());
        canvas.repaint();
    }

    boolean isValidChar(char c)
    {
        return "1234567890".contains(""+c);
    }
    private String validateInputString(String input)
    {
        String outputString = "";
        //returns entire String of valid textBox input
        for(int i = 0; i < input.length(); i++)
        {
            if(isValidChar(input.charAt(i)))
                outputString += input.charAt(i);
        }

        if(outputString.length() == 0)
            outputString = "0";

        /* TODO: Fix input
        if(Integer.parseInt(outputString) > game.getSmallestSide())
            outputString = game.getSmallestSide() + "";
*/
        if(Integer.parseInt(outputString) > 100)
            outputString = "100";

        return outputString;
    }

    private String validateFillSizeInputString(String input)
    {
        String outputString = "";
        //returns entire String of valid textBox input
        for(int i = 0; i < input.length(); i++)
        {
            if(isValidChar(input.charAt(i)))
                outputString += input.charAt(i);
        }

        if(outputString.length() == 0)
            outputString = "0";

        if(Integer.parseInt(outputString) > game.getSmallestSide())
            outputString = game.getSmallestSide() + "";

        return outputString;
    }

    private void clearGraphGraphics()
    {
        populationOverTime.clear();
        graphCanvas.repaint();
        txtMaxPop.setText(TXT_DISP_MARGIN);
        txtMaxGen.setText(TXT_DISP_MARGIN);
        txtMinPop.setText(TXT_DISP_MARGIN);
        txtMinGen.setText(TXT_DISP_MARGIN);
        txtInitialPop.setText(TXT_DISP_MARGIN);
        txtCurrGen.setText(TXT_DISP_MARGIN);
        txtCurrPop.setText(TXT_DISP_MARGIN);

        if(outputPop != null)
            outputPop.updateData(populationOverTime);
    }

    private void updateRateLabel(int updateRate)
    {
        rateLabel.setText("Rate = " + updateRate + " Hz");
    }

    private void handlePopulationInfo()
    {
        //only do this stuff if the population info is drawn
        if(txtMaxPop != null) {
            int max = Collections.max(populationOverTime);
            int maxGen = populationOverTime.indexOf(max);
            int min = Collections.min(populationOverTime);
            int minGen = populationOverTime.indexOf(min);

            ((DrawingGraph) graphCanvas).setGraph(populationOverTime, min, minGen, max, maxGen);

            txtMaxPop.setText(max + TXT_DISP_MARGIN);
            txtMaxGen.setText((maxGen + 1) + TXT_DISP_MARGIN);
            txtMinPop.setText(min + TXT_DISP_MARGIN);
            txtMinGen.setText((minGen + 1) + TXT_DISP_MARGIN);
            txtInitialPop.setText(populationOverTime.get(0) + TXT_DISP_MARGIN);
            txtCurrGen.setText((populationOverTime.size()) + TXT_DISP_MARGIN);
            txtCurrPop.setText(populationOverTime.get(populationOverTime.size() - 1) + TXT_DISP_MARGIN);

            //frame.setTitle("Min pop: "+min+", Max pop: "+max+", Generation "+(populationOverTime.size() - 1)+"'s pop: "+populationOverTime.get(populationOverTime.size() - 1));
        }
    }

    private void changeRunning()
    {
        running = !running;
    }

    private void updateGraphicsStep() {
        printDebug(golDebug, "Drawing step");
        //((DrawingComponent) canvas).enableLoad();
        long timeStart = System.nanoTime();

        //before updating, take current population
        if(drawGraph)
        {
            populationOverTime.add(game.getCurrentAlivePoints().size());
            currentPop = game.getCurrentAlivePoints().size();
            handlePopulationInfo();//DrawingGraph.setGraph() called here
            printDebug(populationDebug, "populationOverTime = " + populationOverTime);

            //have to clear graph
            graphCanvas.repaint();
            if(outputPop != null)
            {
                outputPop.updateData(populationOverTime);
            }
        }

        if(multiplicityDebug)
        {
            System.out.println("Entropy for state = " + calculateEntropy(calculateMultiplicity()));
        }

        if(saveOutput)
        {
            ((DrawingComponent) canvas).saveImage(outputDirectory);
            ((DrawingGraph) graphCanvas).saveImage(outputDirectory);
        }

        ((DrawingComponent) canvas).setGame(game.forwardStep());
        long timeMiddle = System.nanoTime();
        canvas.repaint();
        long timeEnd = System.nanoTime();

        printDebug(golDebug, "Time to calculate step = " + (timeMiddle - timeStart)/1000000L);
        printDebug(golDebug, "Time to draw step = " + (timeEnd - timeMiddle)/1000000L);
    }

    private BigInteger calculateMultiplicity()
    {
        return (game.getAreaFactorial().divide(
                factorial(game.getCurrentAlivePoints().size()).multiply(factorial(game.getArea() - game.getCurrentAlivePoints().size())) ) );
    }

    private double calculateEntropy(BigInteger multiplicity){return Math.log(multiplicity.doubleValue());}

    //helper function
    public static BigInteger factorial(int n)
    {
        if(n > 1)
            return new BigInteger("1");
            // return factorial(n-1).multiply(new BigInteger("" + n));
        else
            return new BigInteger("1");
    }

    private void clickedCoord(int x, int y, boolean paintBlack)
    {
        //first adds or removes point from game
        if(paintBlack)
            game.setTrue(new int[]{x/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getXOffsetDivided(), y/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getYOffsetDivided()});
        else
            game.setFalse(new int[]{x/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getXOffsetDivided(), y/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getYOffsetDivided()});

        ((DrawingComponent) canvas).setPixel(x/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getXOffsetDivided(), y/ SIZE_MULTIPLIER - ((DrawingComponent) canvas).getYOffsetDivided(), paintBlack);
        //((DrawingComponent) canvas).disableLoad();
        //printDebug(golDebug, "Disabled load");
        canvas.repaint();
    }

    private void hoverCoord(int x, int y)
    {
        //draws a gray square over where the mouse is
        ((DrawingComponent) canvas).setHover(x/ SIZE_MULTIPLIER, y/ SIZE_MULTIPLIER);
        ((DrawingComponent) canvas).setRealMousePosition(x, y);
        canvas.repaint();
    }

    private void drawPixel(Point coord, boolean paintBlack)
    {//draw current pixel, no matter what, TODO: maybe make it so it only draws when it moves to a new screenspace pixel
        Point scaledPoint = new Point(coord.x/SIZE_MULTIPLIER, coord.y/SIZE_MULTIPLIER);
        clickedCoord(coord.x, coord.y, paintBlack);
    }

    private void drawPixels(ArrayList<Point> coordList, boolean paintBlack)
    {//only count one hit per inside of bounding box
        for(int i = 0; i < coordList.size(); i++)
        {
            Point scaledPoint = new Point(coordList.get(i).x/SIZE_MULTIPLIER, coordList.get(i).y/SIZE_MULTIPLIER);

            //current game safeguards redundancies
            clickedCoord(coordList.get(i).x, coordList.get(i).y, paintBlack);
        }
    }

    private void timerActionPerformed()
    {
        if(running)
        {//running, update step
            //((DrawingComponent) canvas).enableLoad();
            updateGraphicsStep();
        }
        else
        {//quit loop
            timer.stop();
        }
    }


    private void disableFrame()
    {
        frame.setEnabled(false);

    }
    public void enableFrame()
    {
        frame.setEnabled(true);
    }

    public void updateFromChangeSize(int gw, int gh, int sm, int grw, int grh)
    {
        FRAME_WIDTH = (gw * sm);
        FRAME_HEIGHT = (gh * sm);
        SIZE_MULTIPLIER = sm;

        game.updateSettings(gw, gh);
        ((DrawingComponent) canvas).updateSettings(game.getWidth(), game.getHeight(), SIZE_MULTIPLIER);
        canvas.setSize(FRAME_WIDTH, FRAME_HEIGHT);

        GRAPH_WIDTH = grw;
        GRAPH_HEIGHT = grh;

        if(drawGraph)
            ((DrawingGraph) graphCanvas).updateSize(grw, grh);

        Point loc = frame.getLocation();
        frame.dispose();
        startGUI(loc);

        //make sure that the fill side can't exceed max size
        txtFillSize.setText(validateFillSizeInputString(txtFillSize.getText()));
    }

    //TODO: scale image & set multiplierin ImageConverter
    public void updateFromImageConverter(int sm)
    {
        int gw = game.getWidth();
        int gh = game.getHeight();

        System.out.println(gw);
        System.out.println(gh);

        SIZE_MULTIPLIER = sm;
        FRAME_WIDTH = gw * SIZE_MULTIPLIER;
        FRAME_HEIGHT = gh * SIZE_MULTIPLIER;

        ((DrawingComponent) canvas).resetOffsets();
        ((DrawingComponent) canvas).setGame(game.getCurrentAlivePoints());
        ((DrawingComponent) canvas).updateSettings(gw, gh, SIZE_MULTIPLIER);
        canvas.setSize(FRAME_WIDTH, FRAME_HEIGHT);

        Point loc = frame.getLocation();
        frame.dispose();
        startGUI(loc);

        //make sure that the fill side can't exceed max size
        txtFillSize.setText(validateFillSizeInputString(txtFillSize.getText()));
    }

    public void updateFromScroll(boolean scrollIn)
    {
        //frame stays the same size, but pixels increase/decrease in size
        //want to keep current center in the center later!

        //lots of code copied from ChangeSize
        int oldMult = SIZE_MULTIPLIER;

        int gameWidth = FRAME_WIDTH/SIZE_MULTIPLIER;
        int gameHeight = FRAME_HEIGHT/SIZE_MULTIPLIER;

        //directly change gameWidth and gameHeight

        int newMult = SIZE_MULTIPLIER;

        if(scrollIn)
            newMult -= SCROLL_MULTIPLIER;
        else
            newMult += SCROLL_MULTIPLIER;

        if (newMult <= 0)
            newMult = 1;
        else if (newMult > ChangeSize.MAX_PIXEL_SIZE)
            newMult = ChangeSize.MAX_PIXEL_SIZE;

        if(newMult != oldMult)
        {//only if scrolling changes anything
            //convert the old screen size to new units
            gameWidth = gameWidth * oldMult / newMult;
            gameHeight = gameHeight * oldMult / newMult;

            if (gameWidth < MIN_GAME_WIDTH)
                gameWidth = MIN_GAME_WIDTH;
            if (gameHeight < MIN_GAME_WIDTH)
                gameHeight = MIN_GAME_WIDTH;

            ((DrawingComponent) canvas).scaleFromScroll(gameWidth, gameHeight, newMult);
            SIZE_MULTIPLIER = newMult;
            game.updateSettings(gameWidth, gameHeight);

            canvas.repaint();
        }
    }

    private void launchChangeSize()
    {
        //disable jFrame, will be enabled on close
        disableFrame();
        new ChangeSize(this,
                FRAME_WIDTH/SIZE_MULTIPLIER,
                FRAME_HEIGHT/SIZE_MULTIPLIER,
                SIZE_MULTIPLIER,
                GRAPH_WIDTH,
                GRAPH_HEIGHT);
        stopLoop();
    }

    private void launchSaveBoard()
    {
        stopLoop();
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(
                (System.getProperty("user.home") +
                        System.getProperty("file.separator") +
                        "Documents")));
        int returnVal = fc.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            //put all things that need to be saved into writeObjects
            String filePath = fc.getSelectedFile().getAbsolutePath();
            ArrayList<Serializable> writeObjects = new ArrayList<>();
            writeObjects.add(game.getWidth());
            writeObjects.add(game.getHeight());
            writeObjects.add(SIZE_MULTIPLIER);
            writeObjects.add(game.getCurrentAlivePoints());

            try {
                FileOutputStream fos = new FileOutputStream(filePath);
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                oos.writeObject(writeObjects);

                fos.close();
                oos.close();
            }catch(Exception e){}
        }
    }

    private void launchLoadBoard()
    {
        stopLoop();
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(
                (System.getProperty("user.home") +
                        System.getProperty("file.separator") +
                        "Documents")));
        int returnVal = fc.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            String filePath = fc.getSelectedFile().getAbsolutePath();
            ArrayList<Serializable> readObjects = new ArrayList<>();

            try
            {
                FileInputStream fis = new FileInputStream(filePath);
                ObjectInputStream ois = new ObjectInputStream(fis);

                readObjects = (ArrayList) ois.readObject();

                ois.close();
                fis.close();

                /*
                    readObjects loaded like this:
                        writeObjects.add(game.getWidth());
                        writeObjects.add(game.getHeight());
                        writeObjects.add(SIZE_MULTIPLIER);
                        writeObjects.add(game.getCurrentAlivePoints());
                 */

                game.setAlivePoints((ArrayList<int[]>) readObjects.get(3));
                //updateFromChangeSize(int gw, int gh, int sm, int grw, int grh);
                updateFromChangeSize((int)readObjects.get(0), (int)readObjects.get(1), (int)readObjects.get(2), GRAPH_WIDTH, GRAPH_HEIGHT);
                clearGraphGraphics();
            }catch(Exception e){}
        }
    }

    public void launchOpenImage()
    {
        stopLoop();
        //disableFrame();
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(
            (System.getProperty("user.home") +
            System.getProperty("file.separator") +
            "Pictures")));
        int returnVal = fc.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION)
        {
            String filePath = fc.getSelectedFile().getAbsolutePath();
            ImageConverter ic = new ImageConverter(this, filePath, ((DrawingComponent) canvas), game);
            ic.run();
            //TODO: re-enable: ic = null;
        }
        //enableFrame();
    }

    private void launchHelp()
    {
        stopLoop();
        Help.getInstance(frame);
    }

    private void launchOutputPopulation()
    {
        //stopLoop();
        outputPop = OutputPopulation.getInstance(frame, populationOverTime);
    }

    private int getXOffsetDivided()
    {
        if(canvas != null)
            return ((DrawingComponent) canvas).getXOffsetDivided();
        else
            return 0;
    }
    private int getYOffsetDivided()
    {
        if(canvas != null)
            return ((DrawingComponent) canvas).getYOffsetDivided();
        else
            return 0;
    }

    public JFrame getFrame()
    {
        return frame;
    }
    
    public static void printDebug(boolean control, String input)
    {
        if(control)
            System.out.println(input);
    }

    public Point getFrameLocation()
    {
        return frame.getLocation();
    }

    public boolean getSaveOutput()
    {
        return saveOutput;
    }
}
