import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class DrawingGraph extends Canvas {

    private boolean graphDebug = false;

    private Image dbImage;
    private int imageNumber = 1;

    private int graphWidth, graphHeight;
    private int margin;
    private ArrayList<Integer> graphPoints = new ArrayList<>();
    private int max, min, maxGen, minGen;

    private int POINT_WIDTH = 10;

    public DrawingGraph(int w, int h, int m)
    {
        //graph goes from (m, m) to (w-m, h-m)
        graphWidth = w - (2 * m);
        graphHeight = h - (2 * m);
        margin = m;
        max = -1;
        min = -1;
        maxGen = -1;
        minGen = -1;
    }

    /**
     methods:
     update
     paint
     work together with
     paintComponent
     to create a buffered image to paint to screen
     **/

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        dbImage = createImage(getWidth(), getHeight());
        Graphics dbg = dbImage.getGraphics();
        paintComponent(dbg);
        g.drawImage(dbImage, 0, 0, this);

    }

    public void paintComponent(Graphics g)
    {
        GameOfLife.printDebug(graphDebug, "In graph paint method");

        //first clear background
        g.clearRect(0, 0, graphWidth + (2*margin), graphHeight + (2*margin));

        g.setColor(Color.gray);

        //repaint entire picture

        //paint gridlines on left and right
        g.drawLine(margin, margin, margin, margin + graphHeight);
        g.drawLine(margin, margin + graphHeight, margin + graphWidth, margin + graphHeight);

        //always draw first point on left side y axis
        //fill all other points evenly left to right

        double deltaX = -1;

        //used to draw lines from points, starts at origin
        int prevX = margin;
        int prevY = margin + graphHeight;

        //set deltaX here
        if(!graphPoints.isEmpty()) {
            deltaX = ((double) (graphWidth)) / ((double) (graphPoints.size() - 1));

            //draw gray square over initial

            int x = margin;
            int y = margin + (int) (graphHeight - (((double)graphPoints.get(0)) / ((double) max) * graphHeight));
            g.setColor(Color.GRAY);
            g.fillRect(x-(POINT_WIDTH/2), y-(POINT_WIDTH/2), POINT_WIDTH, POINT_WIDTH);
        }

        g.setColor(Color.GRAY);
        for(int i = 0; i < graphPoints.size(); i++)
        {
            //print scaled point at i * deltaX

            int x = (margin + (int)(i * deltaX));
            int y = margin + (int) (graphHeight - (((double)graphPoints.get(i)) / ((double) max) * graphHeight));

            //draw max and min here in case there are many
            if(graphPoints.get(i) == max)
            {
                g.setColor(Color.green);
                g.fillRect(x - (POINT_WIDTH / 2), y - (POINT_WIDTH / 2), POINT_WIDTH, POINT_WIDTH);
            }
            else if(graphPoints.get(i) == min && min != max)
            {
                g.setColor(Color.red);
                g.fillRect(x-(POINT_WIDTH/2), y-(POINT_WIDTH/2), POINT_WIDTH, POINT_WIDTH);
            }

            g.setColor(Color.gray);
            g.drawLine(prevX, prevY, x, y);
            prevX = x;
            prevY = y;
        }


        if(!graphPoints.isEmpty())
        {
            //draw graph points ONLY ONCE on the TOP!
            /*
            hint, from above:
                int x = (margin + (int)(i * deltaX));
                int y = margin + (int) (graphHeight - (((double)graphPoints.get(i)) / ((double) max) * graphHeight));
             */


            g.setColor(Color.BLACK);
            //draw min stuff
            //this is the X coord
            g.drawString("" + (minGen + 1), (margin + (int)(minGen * deltaX)), (3 * margin / 2) + graphHeight);
            //this is the Y coord
            g.drawString("" + min, (margin / 2), margin + (int)((double) graphHeight * (1.0d - ((double)min) / ((double) max))));

            //draw max stuff
            //this is the X coord
            g.drawString("" + (maxGen + 1), (margin + (int)(maxGen * deltaX)), (3 * margin / 2) + graphHeight);
            //this is the Y coord
            g.drawString("" + max, (margin / 2), margin);

            //draw current generation
            g.drawString("" + (graphPoints.size()), margin + graphWidth, (3*margin/2) + graphHeight);

            //draw initial population
            g.drawString("" + graphPoints.get(0), (margin / 2), margin + (int)((double) graphHeight * (1.0d - ((double)graphPoints.get(0)) / ((double) max))));
        }
    }

    //graphPoints is a set of all the points
    public void setGraph(ArrayList<Integer> inputGraphPoints, int mi, int miG, int ma, int maG)
    {
        graphPoints = inputGraphPoints;
        min = mi;
        minGen = miG;
        max = ma;
        maxGen = maG;
    }

    public void updateSize(int w, int h)
    {
        graphWidth = w - (2 * margin);
        graphHeight = h - (2 * margin);
    }

    public void saveImage(String directory)
    {
        try {
            BufferedImage saveImage = (BufferedImage) dbImage;
            File outputfile = new File(directory + "/" + "Population" + imageNumber + ".png");
            ImageIO.write(saveImage, "png", outputfile);
            imageNumber++;
        } catch (IOException e) {
        }
    }
}
