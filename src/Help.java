import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Help extends JFrame implements CustomIcon{
    private static Help instance = null;

    private int FRAME_WIDTH = 650;
    private int FRAME_HEIGHT = 500;

    private JPanel contentPane;
    private JButton btnClose;
    private JLabel txtDisplay;
    private JScrollPane scrollP;
    private JLabel txtIcon;

    public Help(JFrame golFrame) {
        setIconImage(GameOfLife.icon.getImage());
        setAlwaysOnTop(true);
        setResizable(false);
        setTitle("Help");
        setContentPane(contentPane);
        getRootPane().setDefaultButton(btnClose);

        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        });
        // call onClose() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        // call onClose() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        startGUI(golFrame);
    }

    private void startGUI(JFrame golFrame)
    {
        //pack();
        contentPane.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        contentPane.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        contentPane.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));

        //scrollP.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        //scrollP.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        //scrollP.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));

        ArrayList<String> helpMessage = new ArrayList<>();
        helpMessage.add("<h2>General</h2>This program is a version of <b>Conway's Game of Life</b> and follows these rules (taken from Wikipedia):");
        String listString = "<ol>";
        listString += "<li>Any live cell with fewer than two live neighbours dies, as if by underpopulation.</li>";
        listString += "<li>Any live cell with two or three live neighbours lives on to the next generation.</li>";
        listString += "<li>Any live cell with more than three live neighbours dies, as if by overpopulation.</li>";
        listString += "<li>Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.</li>";
        listString += "(Any cell outside the boundaries of the game are considered to be dead.)";
        listString += "</ol>";
        helpMessage.add(listString + "<b>Left click</b> to give a cell life, <b>right click</b> to take it away.");
        helpMessage.add("The <b>Step</b> button advances the game one time step into the future, and the <b>Start</b> button is equivalent");
        helpMessage.add("   &emsp;&emsp;to pressing the Step button at the rate given.");
        helpMessage.add("Change the <b>rate</b> of the Start button by using the slider to the right of it.");
        helpMessage.add("The <b>Clear</b> button erases the current game and graph.");
        helpMessage.add("The <b>Fill Percentage</b> button fills the inputted percentage of cells.");

        helpMessage.add("<h2>Settings</h2>The <b>Settings</b> menu contains options for:");
        listString = "<ol>";
        listString += "<li>Altering dimensions</li>";
        listString += "<li>Enabling/Disabling a background grid</li>";
        listString += "<li>Showing current coordinates</li>";
        listString += "<li>Graphing the population over time</li>";
        listString += "</ol>";

        helpMessage.add(listString + "<h2>Altering Dimensions</h2>The <b>Grid Width</b> and <b>Grid Height</b> fields set the dimensions of the game space.");
        helpMessage.add("The <b>Cell Size</b> field sets the length of a single cell in pixels.");
        helpMessage.add("<b>* Note:</b> Any combination of <b>Grid Width</b>, <b>Grid Height</b> and <b>Cell Size</b> must create a window smaller than");
        helpMessage.add("   &emsp;&emsp;1500x750 pixels.");
        helpMessage.add("The <b>Graph Width</b> and <b>Graph Height</b> fields set the size of the Population vs Time graph, if it is drawn.");
        helpMessage.add("Hitting the <b>Enter</b> key anywhere in this window will confirm the inputted dimensions.");



        helpMessage.add("<br><b>* Tip:</b> Leaving the <b>Grid Width</b> and <b>Grid Height</b> fields empty while changing the <b>Cell Size</b> will keep the");
        helpMessage.add("    &emsp;&emsp;size of the window fixed.");

        helpMessage.add("<h2>Graph Population</h2>The <b>graph</b> records the <b>initial population</b>, current population, <b>maximum population</b>, and <b>minimum");
        helpMessage.add("   &emsp;&emsp;population</b> of the game.");
        helpMessage.add("The <b>gray</b> point represents the initial population, the <b>green</b> point represents the maximum population,");
        helpMessage.add("   &emsp;&emsp;and the <b>red</b> point represents the minimum population.");
        helpMessage.add("<b>Max pop</b> and <b>Min pop</b> only record the first instance of each population.");
        helpMessage.add("The <b>Output Population</b> button outputs the current graph to a list of its coordinates.");
        helpMessage.add("The <b>Clear Graph</b> button erases the current graph.");

        helpMessage.add("<b>* Note:</b> Allowing the graph to run for ~4000 generations starts to negatively impact the responsiveness of");
        helpMessage.add("   &emsp;&emsp;the whole program, so it is best to clear it before then.");

        helpMessage.add("<br>");
        helpMessage.add("Created by Desmond Soroka 2019<br>");

        helpMessage.add("Yes, this program's icon is an image of Jon Arbuckle, the Garfield Character<br>");



        txtDisplay.setText(strArrToHTML(helpMessage));

        pack();
        setLocationRelativeTo(golFrame);
        setVisible(true);
    }



    private void onClose() {
        // add your code here
        instance = null;
        dispose();
    }

    private String strArrToHTML(ArrayList<String> strs)
    {
        String strToReturn = "<html><body width='%1s'>";

        for(int i = 0; i < strs.size(); i++)
        {
            strToReturn += strs.get(i);
            strToReturn += "<br>";
        }

        //strToReturn = strToReturn.substring(0, strToReturn.length() - 4);

        strToReturn += "</html>";

        return strToReturn;
    }

    public static Help getInstance(JFrame parentFrame)
    {
        if (instance == null)
            instance = new Help(parentFrame);

        return instance;
    }

    private void createUIComponents() {
        scrollP = new JScrollPane(contentPane,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollP.getVerticalScrollBar().setUnitIncrement(12);
        txtIcon = new JLabel(new ImageIcon(icon.getImage()));
    }
}
